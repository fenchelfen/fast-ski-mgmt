# fast api project


## Overview

This is a [Fast API](https://fastapi.tiangolo.com/project-generation/) application for ordering transfers of skis. It uses `httpx` to make asynchronous queries to external APIs. This is done through a single endpoint.

```/api/v1/inventory/baggage-management/skis/```

This app is tested in Gitlab CI via [pytest](https://pytest.org) and [pytest-httpx](https://colin-b.github.io/pytest_httpx/) and uses [Poetry](https://python-poetry.org) as its package manager.


## Local environment

1. Copy `.env.example` into `.env` 
2. Uncomment respective lines in `docker-compose.override.yml`
3. Run `docker-compose up`

You can try it out at `localhost/docs` now.

## Running tests

I use `pytest_httpx` to mock external API calls (see [here](https://gitlab.com/fenchelfen/fast-ski-mgmt/-/blob/main/backend/app/app/tests/api/api_v1/test_baggage_mgmt.py) for more details). Use the following command
to execute tests in a running container.

```bash
docker-compose up
docker exec -ti fast-ski-mgmt_backend_1 poetry run pytest
```


