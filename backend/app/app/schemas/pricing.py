from typing import List, Optional
from typing_extensions import Literal

from pydantic import BaseModel


class Weight(BaseModel):
    amount: int
    unit: Literal['KG']


class Baggage(BaseModel):
    id: str
    overWeight: bool
    amount: int
    unit: str
    weight: Optional[Weight]
    code: str
    descriptions: List[str]
    registered: bool
    equipmentType: Optional[str]


class BaggagePricing(BaseModel):
    passengerIds: List[str]
    passengerTypes: List[Literal['ADT', 'KID']]
    purchaseType: Literal['PAID', 'UNPAID']
    routeId: str
    baggages: List[Baggage]


class AncillariesPricing(BaseModel):
    airId: str
    baggagePricings: List[BaggagePricing]
    baggageDisabled: bool
    seatsDisabled: bool
    mealsDisabled: bool
    upgradesDisabled: bool
    loungesDisabled: bool
    fastTracksDisabled: bool
    petsDisabled: bool


class AncillariesPricings(BaseModel):
    ancillariesPricings: List[AncillariesPricing]