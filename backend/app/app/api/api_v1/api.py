from fastapi import APIRouter

from app.api.api_v1.endpoints import baggage

api_router = APIRouter()
api_router.include_router(baggage.router, prefix="/inventory", tags=["inventory"])
